package com.conygre.training.tradesimulator.rest.funds;

import java.util.Optional;

import com.conygre.training.tradesimulator.model.funds.Funds;
import com.conygre.training.tradesimulator.service.funds.FundsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/funds")
@CrossOrigin // allows requests from all domains
public class FundsController {
    private final String USER = "user";

    @Autowired
    private FundsService service;
    public FundsController(FundsService service) {
		  this.service = service;
    }
    @RequestMapping(method = RequestMethod.GET)
	  public Optional<Funds> findAll() {
		// logger.info("managed to call a Get request for findAll");
		  return service.getFunds(USER);
    }
}
