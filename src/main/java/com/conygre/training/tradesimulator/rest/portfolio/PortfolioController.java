package com.conygre.training.tradesimulator.rest.portfolio;

import com.conygre.training.tradesimulator.model.portfolio.Portfolio;
import com.conygre.training.tradesimulator.service.portfolio.PortfolioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/portfolio")
@CrossOrigin // allows requests from all domains
public class PortfolioController {
    @Autowired
    private PortfolioService service;
    public PortfolioController(PortfolioService service) {
		  this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
	  public Iterable<Portfolio> findAll() {
		// logger.info("managed to call a Get request for findAll");
		  return service.getPortfolios();
    }
}
