package com.conygre.training.tradesimulator.dao.funds;

import com.conygre.training.tradesimulator.model.funds.Funds;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface FundsMongoDao extends MongoRepository<Funds, String> {

}