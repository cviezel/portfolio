package com.conygre.training.tradesimulator.dao.portfolio;

import com.conygre.training.tradesimulator.model.portfolio.Portfolio;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PortfolioMongoDao extends MongoRepository<Portfolio, String> {

}