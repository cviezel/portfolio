package com.conygre.training.tradesimulator.model.portfolio;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Portfolio {

    @Id
    private String id;
    private int quantity;

    public Portfolio(String id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }
   
    public Portfolio() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

	public int getQuantity() {
		return quantity;
	}

}