package com.conygre.training.tradesimulator.model.trade;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id;
    private String type;
    private String dateCreated;
    private String ticker;
    private int quantity;
    private double requestedPrice;
    private String state;

    public Trade(String type, String ticker, int quantity, double requestedPrice) {
        this();
        this.ticker = ticker;
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
    }
   
    public Trade() {
        Date now = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        dateCreated = ft.format(now);
        state = TradeState.CREATED.toString();
    }

    public String getHexString() {
        return id.toHexString();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(TradeState tradeStatus) {
        this.state = tradeStatus.toString();
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}