package com.conygre.training.tradesimulator.model.funds;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Funds {

    @Id
    private String id;
    private double funds;

    public Funds(String id, double funds) {
        this.id = id;
        this.funds = funds;
    }
   
    public Funds() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFunds(double funds) {
        this.funds = funds;
    }

	public double getFunds() {
		return funds;
	}

}