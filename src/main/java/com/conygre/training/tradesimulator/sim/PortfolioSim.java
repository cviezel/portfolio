package com.conygre.training.tradesimulator.sim;

import java.util.Comparator;
import java.util.List;

import com.conygre.training.tradesimulator.dao.trade.TradeMongoDao;
import com.conygre.training.tradesimulator.model.trade.Trade;
import com.conygre.training.tradesimulator.model.trade.TradeState;
import com.conygre.training.tradesimulator.service.funds.FundsService;
import com.conygre.training.tradesimulator.service.portfolio.PortfolioService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PortfolioSim {
    private static final Logger LOG = LoggerFactory.getLogger(PortfolioSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private FundsService fundsService;

    @Transactional
    public List<Trade> addFilledTradesToPortfolio(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.FILLED);
        foundTrades.sort(Comparator.comparing(Trade::getDateCreated));
        for(Trade thisTrade: foundTrades) {
            tradeDao.save(thisTrade);
            
            String ticker = thisTrade.getTicker();
            int quantity = thisTrade.getQuantity();
            double requestedPrice = thisTrade.getRequestedPrice();
            String type = thisTrade.getType();

            double totalCost = requestedPrice * quantity;

            if(type.equals("BUY")) {
                //Make sure the user can afford the transaction
                if(fundsService.subFunds(totalCost)) {
                    //Buy the stock and subtract the funds
                    portfolioService.buyStock(ticker, quantity);
                    thisTrade.setState(TradeState.COMPLETED);
                }
                else {
                    //Reject the trade because of insufficient funds
                    thisTrade.setState(TradeState.REJECTED);
                }
                
            }
            else if(type.equals("SELL")) {
                //Make sure the user has enough stock that they are trying to sell
                if(portfolioService.sellStock(ticker, quantity)) {
                    //Add the funds to their account
                    thisTrade.setState(TradeState.COMPLETED);
                    fundsService.addFunds(totalCost);
                }
                else {
                    //Reject the trade
                    thisTrade.setState(TradeState.REJECTED);
                }
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        LOG.debug("Main loop running!");
        
        int processedTrades = addFilledTradesToPortfolio().size();
        LOG.debug("Found " + processedTrades + " trades to be processed");

    }
}
