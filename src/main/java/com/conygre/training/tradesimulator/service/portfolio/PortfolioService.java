package com.conygre.training.tradesimulator.service.portfolio;

import java.util.Collection;
import java.util.Optional;

import com.conygre.training.tradesimulator.dao.portfolio.PortfolioMongoDao;
import com.conygre.training.tradesimulator.model.portfolio.Portfolio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortfolioService {
    
    @Autowired
    private PortfolioMongoDao repo;


    public boolean buyStock(String ticker, int quantity) {
        //Check to see if the user has a particular stock
        Optional<Portfolio> ret = getPortfolio(ticker);
        if(ret.isPresent()) {
            //Increment the stock count
            Portfolio portfolio = ret.get();
            portfolio.setQuantity(quantity + portfolio.getQuantity());
            repo.save(portfolio);
        }
        else {
            //Create an entry in portfolio with the requested quantity
            Portfolio portfolio = new Portfolio(ticker, quantity);
            repo.save(portfolio);
        }
        return true;
    }
    public boolean sellStock(String ticker, int quantity) {
        //Check to see if the user has a particular stock
        Optional<Portfolio> ret = getPortfolio(ticker);
        if(ret.isPresent()) {
            Portfolio portfolio = ret.get();
            //Check to see if the user has more quantity than they are trying to sell
            if(portfolio.getQuantity() - quantity >= 0) {
                portfolio.setQuantity(portfolio.getQuantity() - quantity);
                repo.save(portfolio);
                return true;
            }
        }
        //If the user does not have enough stock or doesn't own the stock, return false
        return false;
    }
    //Gets all the portfolios
    public Collection<Portfolio> getPortfolios() {
        return repo.findAll();
    }
    //Gets a particular portfolio
    public Optional<Portfolio> getPortfolio(String id) {
        return repo.findById(id);
    }
}