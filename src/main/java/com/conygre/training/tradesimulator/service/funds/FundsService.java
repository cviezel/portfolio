package com.conygre.training.tradesimulator.service.funds;

import java.util.Optional;

import com.conygre.training.tradesimulator.dao.funds.FundsMongoDao;
import com.conygre.training.tradesimulator.model.funds.Funds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FundsService {
    private final String USER = "user";
    @Autowired
    private FundsMongoDao repo;

    //Gets the fund by id
    public Optional<Funds> getFunds(String id) {
        return repo.findById(id);
    }
    public boolean addFunds(double money) {
        Optional<Funds> ret = getFunds(USER);
        //If the user has funds add to it
        if(ret.isPresent()) {
            Funds funds = ret.get();
            funds.setFunds(funds.getFunds() + money);
            repo.save(funds);
        }
        //If the user has no entry then create one
        else {
            Funds funds = new Funds(USER, money);
            repo.save(funds);
        }
        return true;
    } 
    public boolean subFunds(double money) {
        Optional<Funds> ret = getFunds(USER);
        //If the user has funds, subtract from them
        if(ret.isPresent()) {
            Funds funds = ret.get();
            if((funds.getFunds() - money) >= 0) {
                funds.setFunds(funds.getFunds() - money);
                repo.save(funds);
                return true;
            }
            return false;
        }
        else {
            //If not, give them a default value
            Funds funds = new Funds(USER, 1000);
            repo.save(funds);
            return true;
        }
    }
}
