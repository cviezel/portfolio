package com.conygre.training.tradesimulator.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.conygre.training.tradesimulator.model.funds.Funds;
import com.conygre.training.tradesimulator.rest.funds.FundsController;
import com.conygre.training.tradesimulator.service.funds.FundsService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FundsControllerUnitTests {
    private FundsController controller;

    @BeforeEach
    public void start() {
        String id = "user";
        int price = 700;
        Funds f = new Funds(id, price);
        List<Funds> fundsList = new ArrayList<>();
        fundsList.add(f);

        FundsService service = mock(FundsService.class);
        when(service.getFunds(id)).thenReturn(Optional.of(f));

        controller = new FundsController(service);
    }

    @Test
    public void testFind() {
        Optional<Funds> funds = controller.findAll();
        assertThat(funds.isPresent(), equalTo(true));
    }
}
