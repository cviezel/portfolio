package com.conygre.training.tradesimulator.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.training.tradesimulator.model.portfolio.Portfolio;
import com.conygre.training.tradesimulator.rest.portfolio.PortfolioController;
import com.conygre.training.tradesimulator.service.portfolio.PortfolioService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PortfolioControllerUnitTests {
    private PortfolioController controller;

    @BeforeEach
    public void start() {
        String id = "IBM";
        int quantity = 2;
        Portfolio p = new Portfolio(id, quantity);
        List<Portfolio> portfolioList = new ArrayList<>();
        portfolioList.add(p);

        PortfolioService service = mock(PortfolioService.class);
        when(service.getPortfolios()).thenReturn(portfolioList);
        when(service.getPortfolio(id)).thenReturn(Optional.of(p));

        controller = new PortfolioController(service);
    }

    @Test
    public void testFindAll() {
        Iterable<Portfolio> trades = controller.findAll();
        Stream<Portfolio> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
}