# Portfolio Engine

### Outline

On a timed interval it will read from a MongoDB database. It will look for records that represent Trade objects with a "FILLED" status.

It will mark trades as "REJECTED" if the trade cannot be completed (Not enough funds, user sells stock they do not have, etc...)

It will accept trades that are valid and mark them as "COMPLETED". It will store records in 2 MongoDB databases to actively record the user's current portfolio and funds.

### Run
Build on the command line with gradle:
* ./gradlew build

This will put a jar in build/libs/portfolio-0.0.1-SNAPSHOT.jar

Run that jar with:
* java -jar build/libs/trade-simulator-0.0.1-SNAPSHOT.jar

OR for example with DEBUG logging and on a different port (8089):
* java -DLOG_LEVEL=DEBUG -DSERVER_PORT=8089 -jar build/libs/trade-simulator-0.0.1-SNAPSHOT.jar


OR just run in VSCode or any other IDE for development

### Docker
There is a Dockerfile included to run as a Docker container.

e.g. to build a container:
* docker build -t portfolio:0.0.1 .

e.g. to run the built container with DEBUG logging:
* docker run --name portfolio -e LOG_LEVEL=DEBUG trade-sim:0.0.1 

### Configuration
See the properties file in src/main/resources/application.properties for configurable properties.

Most properties can be overridden by environmental variables.
